class Article < ApplicationRecord
    validates :title, presence:true, length:{minimum:2, maximum:30 }
    validates :description, presence:true, length:{minimum:2, maximum:150 }
    
  
end  